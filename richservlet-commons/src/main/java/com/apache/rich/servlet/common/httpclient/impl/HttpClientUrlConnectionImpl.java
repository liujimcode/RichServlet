package com.apache.rich.servlet.common.httpclient.impl;

import com.apache.rich.servlet.common.httpclient.HttpClient;
import com.apache.rich.servlet.common.httpclient.HttpRequest;
import com.apache.rich.servlet.common.httpclient.HttpResponse;
import com.apache.rich.servlet.common.httpclient.impl.UrlMultipartEntity;
import com.apache.rich.servlet.common.utils.FileUtils;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Iterator;
import java.util.Map.Entry;

public class HttpClientUrlConnectionImpl implements HttpClient {
	private static final int BUFFER_SIZE = 1024;
	private static final String METHOD_POST = "POST";

	public HttpResponse getDowload(HttpRequest request) {
		try {
			URL e = new URL(request.getGetUrl());
			HttpURLConnection response1 = (HttpURLConnection) e
					.openConnection();
			this.initRequest(response1, request);
			return readResponseForFile(response1, request);
		} catch (Exception arg3) {
			arg3.printStackTrace();
			HttpResponse response = new HttpResponse();
			response.setStatusCode(-1);
			response.setReasonPhrase(arg3.getMessage());
			return response;
		}
	}

	public HttpResponse postDowload(HttpRequest request) {
		try {
			URL e = new URL(request.getUrl());
			HttpURLConnection response1 = (HttpURLConnection) e
					.openConnection();
			response1.setRequestMethod("POST");
			response1.setDoOutput(true);
			response1.setUseCaches(false);
			this.initRequest(response1, request);
			putParamsToOutputStream(response1, request);
			return readResponseForFile(response1, request);
		} catch (Exception arg3) {
			HttpResponse response = new HttpResponse();
			response.setStatusCode(-1);
			response.setReasonPhrase(arg3.getMessage());
			return response;
		}
	}

	public HttpResponse get(HttpRequest request) {
		try {
			URL e = new URL(request.getGetUrl());
			HttpURLConnection response1 = (HttpURLConnection) e
					.openConnection();
			this.initRequest(response1, request);
			return readResponseForString(response1, request.getEncode());
		} catch (Exception arg3) {
			HttpResponse response = new HttpResponse();
			response.setStatusCode(-1);
			response.setReasonPhrase(arg3.getMessage());
			return response;
		}
	}

	public HttpResponse post(HttpRequest request) {
		try {
			URL e = new URL(request.getUrl());
			HttpURLConnection response1 = (HttpURLConnection) e
					.openConnection();
			response1.setRequestMethod("POST");
			response1.setDoOutput(true);
			response1.setUseCaches(false);
			this.initRequest(response1, request);
			putParamsToOutputStream(response1, request);
			return readResponseForString(response1, request.getEncode());
		} catch (Exception arg3) {
			HttpResponse response = new HttpResponse();
			response.setStatusCode(-1);
			response.setReasonPhrase(arg3.getMessage());
			return response;
		}
	}

	public HttpResponse postJson(HttpRequest request) {
		try {
			URL e = new URL(request.getUrl());
			HttpURLConnection response1 = (HttpURLConnection) e
					.openConnection();
			response1.setRequestMethod("POST");
			response1.setDoOutput(true);
			this.initRequest(response1, request);
			putParamsToOutputStreamForJson(response1, request);
			return readResponseForString(response1, request.getEncode());
		} catch (Exception arg3) {
			HttpResponse response = new HttpResponse();
			response.setStatusCode(-1);
			response.setReasonPhrase(arg3.getMessage());
			return response;
		}
	}

	public HttpResponse upload(HttpRequest request) {
		try {
			URL e = new URL(request.getUrl());
			HttpURLConnection response1 = (HttpURLConnection) e
					.openConnection();
			this.initRequest(response1, request);
			putBodyEntityToOutputStream(response1, request);
			return readResponseForString(response1, request.getEncode());
		} catch (Exception arg3) {
			HttpResponse response = new HttpResponse();
			response.setStatusCode(-1);
			response.setReasonPhrase(arg3.getMessage());
			return response;
		}
	}

	private void initRequest(URLConnection conn, HttpRequest request) {
		Iterator arg2 = request.getHeaderMap().entrySet().iterator();

		while (arg2.hasNext()) {
			Entry entry = (Entry) arg2.next();
			conn.addRequestProperty((String) entry.getKey(),
					(String) entry.getValue());
		}

		conn.setConnectTimeout(request.getConnectionTimeout());
		conn.setReadTimeout(request.getReadTimeout());
	}

	private static void putBodyEntityToOutputStream(HttpURLConnection conn,
			HttpRequest request) {
		try {
			UrlMultipartEntity e = new UrlMultipartEntity();
			e.writeDataToBody(conn, request);
		} catch (Exception arg2) {
			System.out.println(arg2.getMessage());
		}

	}

	private static void putParamsToOutputStreamForJson(HttpURLConnection conn,
			HttpRequest request) {
		String bodyJson = request.getBodyJson();
		DataOutputStream dos = null;

		try {
			dos = new DataOutputStream(conn.getOutputStream());
			dos.write(bodyJson.getBytes());
		} catch (Exception arg12) {
			System.out.println(arg12.getMessage());
		} finally {
			try {
				dos.flush();
				dos.close();
			} catch (Exception arg11) {
				;
			}

		}

	}

	private static void putParamsToOutputStream(HttpURLConnection conn,
			HttpRequest request) {
		String paramStr = request.getParamsStr();
		DataOutputStream dos = null;

		try {
			dos = new DataOutputStream(conn.getOutputStream());
			dos.write(paramStr.getBytes());
		} catch (Exception arg12) {
			;
		} finally {
			try {
				dos.flush();
				dos.close();
			} catch (Exception arg11) {
				;
			}

		}

	}

	private static HttpResponse readResponseForFile(HttpURLConnection conn,
			HttpRequest request) {
		boolean hasListener = false;
		if (null != request.getDownloadListener()) {
			hasListener = true;
		}

		HttpResponse response = new HttpResponse();
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		InputStream inStream = null;

		try {
			inStream = conn.getInputStream();
			byte[] e = new byte[1024];
			boolean len = false;
			int curCount = 0;
			int total = -1;
			if (hasListener) {
				total = conn.getContentLength();
			}

			int len1;
			for (; (len1 = inStream.read(e)) != -1; outStream.write(e, 0, len1)) {
				curCount += len1;
				if (hasListener) {
					request.getDownloadListener().callBack((long) total,
							(long) curCount, false);
				}
			}

			if (hasListener) {
				request.getDownloadListener().callBack((long) total,
						(long) curCount, true);
			}

			response.setStatusCode(conn.getResponseCode());
			response.setReasonPhrase(conn.getResponseMessage());
			byte[] data = outStream.toByteArray();
			File file = new File(request.getDownloadFileName());
			FileUtils.writeByteArrayToFile(file, data, false);
			response.setResultFile(file);
		} catch (Exception arg19) {
			response.setStatusCode(-1);
			response.setReasonPhrase(arg19.getMessage());
		} finally {
			try {
				outStream.close();
				inStream.close();
			} catch (Exception arg18) {
				;
			}

		}

		return response;
	}

	private static HttpResponse readResponseForString(HttpURLConnection conn,
			String encode) {
		HttpResponse response = new HttpResponse();
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		InputStream inStream = null;

		try {
			inStream = conn.getInputStream();
			byte[] e = new byte[1024];
			boolean len = false;

			int len1;
			while ((len1 = inStream.read(e)) != -1) {
				outStream.write(e, 0, len1);
			}

			byte[] data = outStream.toByteArray();
			response.setStatusCode(conn.getResponseCode());
			response.setReasonPhrase(conn.getResponseMessage());
			response.setResultStr(new String(data, encode));
		} catch (Exception arg15) {
			response.setStatusCode(-1);
			response.setReasonPhrase(arg15.getMessage());
		} finally {
			try {
				outStream.close();
				inStream.close();
			} catch (Exception arg14) {
				;
			}

		}

		return response;
	}
}