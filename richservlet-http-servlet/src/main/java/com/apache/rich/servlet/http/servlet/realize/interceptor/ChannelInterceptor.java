
package com.apache.rich.servlet.http.servlet.realize.interceptor;

import com.apache.rich.servlet.http.servlet.realize.ChannelThreadLocal;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpResponse;

/**
 * 
 * @author wanghailing
 *
 */
public class ChannelInterceptor implements RichServletInterceptor {

    @Override
    public void onRequestFailed(ChannelHandlerContext ctx, Throwable e,
                                HttpResponse response) {
        ChannelThreadLocal.unset();
    }

    @Override
    public void onRequestReceived(ChannelHandlerContext ctx, HttpRequest e) {
        ChannelThreadLocal.set(ctx.channel());
    }

    @Override
    public void onRequestSuccessed(ChannelHandlerContext ctx, HttpRequest e,
                                   HttpResponse response) {
        ChannelThreadLocal.unset();
    }

}
