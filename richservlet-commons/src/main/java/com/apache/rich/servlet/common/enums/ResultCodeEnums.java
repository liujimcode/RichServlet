package com.apache.rich.servlet.common.enums;

public enum ResultCodeEnums {
	SUCCESS, PARAMS_NOT_MATCHED, PARAMS_CONVERT_ERROR, RESPONSE_NOT_VALID, SYSTEM_ERROR;
}