/**
 * 
 */
package com.apache.rich.servlet.http.servlet.realize;

import com.apache.rich.servlet.http.servlet.realize.interceptor.RichServletHttpSessionInterceptor;
import com.apache.rich.servlet.http.servlet.realize.interceptor.ChannelInterceptor;
import com.apache.rich.servlet.http.servlet.server.handler.RichServletAsyncHttpServletHandler;
import com.apache.rich.servlet.http.servlet.realize.configuration.RichServletHttpWebappConfiguration;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.concurrent.DefaultEventExecutor;
import com.apache.rich.servlet.http.servlet.realize.session.RichServletHttpSessionStore;
import com.apache.rich.servlet.http.servlet.realize.session.RichServletHttpSessionStoreImpl;

/**
 * 
 * @author wanghailing
 *
 */
public class RichServletHttpServlet {
	
	private DefaultEventExecutor eventExecutor = new DefaultEventExecutor();

    private ChannelGroup allChannels = new DefaultChannelGroup(eventExecutor);

    private HttpSessionWatchdog watchdog;
    
    private String uriPrefix = "/"; 

    public RichServletHttpServlet(RichServletHttpWebappConfiguration webappConfiguration,String uriPrefix){
        
        RichServletHttpServletWebapp webapp = RichServletHttpServletWebapp.get();
        webapp.init(webappConfiguration, allChannels);
        this.uriPrefix=uriPrefix;
        new Thread(this.watchdog = new HttpSessionWatchdog()).start();
    }
    
    public RichServletHttpServlet(RichServletHttpWebappConfiguration webappConfiguration){
    		RichServletHttpServletWebapp webapp = RichServletHttpServletWebapp.get();
        webapp.init(webappConfiguration, allChannels);
        new Thread(this.watchdog = new HttpSessionWatchdog()).start();
    }
    
    public void shutdown() {
        this.watchdog.stopWatching();
        RichServletHttpServletWebapp.get().destroy();
        this.allChannels.close().awaitUninterruptibly();
    }
	
    public RichServletAsyncHttpServletHandler getRichServletAsyncHttpRequestHandler(){
    		RichServletAsyncHttpServletHandler asyncHttpRequestHandler=RichServletAsyncHttpServletHandler.build(uriPrefix);
    		asyncHttpRequestHandler.addInterceptor(new ChannelInterceptor());
    		asyncHttpRequestHandler.addInterceptor(new RichServletHttpSessionInterceptor(
                    getHttpSessionStore()));
    		return asyncHttpRequestHandler;
    }
    
	private class HttpSessionWatchdog implements Runnable {

        private boolean shouldStopWatching = false;

        @Override
        public void run() {

            while (!shouldStopWatching) {

                try {
                		RichServletHttpSessionStore store = getHttpSessionStore();
                    if (store != null) {
                        store.destroyInactiveSessions();
                    }
                    Thread.sleep(5000);

                } catch (InterruptedException e) {
                    return;
                }

            }

        }
        
        public void stopWatching() {
            this.shouldStopWatching = true;
        }
	}
	
	protected RichServletHttpSessionStore getHttpSessionStore() {
        return new RichServletHttpSessionStoreImpl();
    }

	
}
