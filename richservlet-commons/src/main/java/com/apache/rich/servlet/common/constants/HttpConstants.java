package com.apache.rich.servlet.common.constants;

public class HttpConstants {
	public static final String HEADER_X_FORWARDED_FOR = "X-Forwarded-For";
	public static final String HEADER_CONTENT_LENGTH = "Content-Length";
	public static final String HEADER_CONTENT_TYPE = "Content-Type";
	public static final String HEADER_CONNECTION = "Connection";
	public static final String HEADER_SERVER = "Server";
	public static final String HEADER_REQUEST_ID = "Request-Id";
	public static final String HEADER_CONNECTION_KEEPALIVE = "keep-alive";
	public static final String HEADER_CONNECTION_CLOSE = "close";
	public static final String HEADER_CONTENT_TYPE_TEXT = "application/text; charset=UTF-8";
	public static final String HEADER_CONTENT_TYPE_TEXT_PLAIN = "text/html; charset=UTF-8";
	public static final String HEADER_CONTENT_TYPE_JSON = "application/json; charset=UTF-8";
	public static final String ACCESS_CONTROL_ALLOW_ORIGIN = "Access-Control-Allow-Origin";
	public static final String CACHE_CONTROL = "Cache-Control";
	public static final String PRAGMA = "Pragma";
	public static final String EXPIRES = "Expires";
}