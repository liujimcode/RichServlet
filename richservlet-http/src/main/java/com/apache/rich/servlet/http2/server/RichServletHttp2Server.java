/**
 * 
 */
package com.apache.rich.servlet.http2.server;

import com.apache.rich.servlet.core.server.acceptor.IOAcceptor;
import com.apache.rich.servlet.core.server.RichServletServer;

/**
 * @author wanghailing
 *
 */
public class RichServletHttp2Server extends RichServletServer {
	
	private IOAcceptor ioAcceptor;
	
	public RichServletHttp2Server ioAcceptor(IOAcceptor asyncAcceptor) {
        this.ioAcceptor = asyncAcceptor;
        return this;
    }
	/* (non-Javadoc)
	 * @see com.apache.rich.servlet.core.server.Server#start()
	 */
	@Override
	public Boolean start() {
		try {
            // run event loop and joined
            ioAcceptor.eventLoop();
            return Boolean.TRUE;
        } catch (Exception e) {
            e.printStackTrace();
            return Boolean.FALSE;
        }
	}

	/* (non-Javadoc)
	 * @see com.apache.rich.servlet.core.server.Server#join()
	 */
	@Override
	public void join() throws InterruptedException {
        ioAcceptor.join();
	}

	/* (non-Javadoc)
	 * @see com.apache.rich.servlet.core.server.Server#shutdown()
	 */
	@Override
	public void shutdown() {
		ioAcceptor.shutdown();									
	}

}
