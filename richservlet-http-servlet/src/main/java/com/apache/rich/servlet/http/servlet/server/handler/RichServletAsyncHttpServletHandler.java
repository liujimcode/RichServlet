package com.apache.rich.servlet.http.servlet.server.handler;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;

import com.apache.rich.servlet.common.constants.RichServletServerConstants;

import io.netty.handler.codec.http.HttpHeaderValues;
import io.netty.handler.codec.http.HttpHeaderNames;
import static io.netty.handler.codec.http.HttpResponseStatus.OK;
import com.apache.rich.servlet.common.constants.HttpConstants;
import com.apache.rich.servlet.http.servlet.realize.utils.HttpServletUtils;
import io.netty.handler.stream.ChunkedFile;
import io.netty.channel.FileRegion;
import io.netty.channel.DefaultFileRegion;
import io.netty.channel.ChannelProgressiveFutureListener;
import io.netty.channel.ChannelProgressiveFuture;
import io.netty.handler.codec.http.HttpMethod;
import com.apache.rich.servlet.http.servlet.realize.RichServletHttpServletRequest;
import com.apache.rich.servlet.http.servlet.realize.RichservletHttpFilterChain;
import com.apache.rich.servlet.http.servlet.realize.RichServletHttpServletResponse;
import io.netty.channel.Channel;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.ssl.SslHandler;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpResponse;
import io.netty.handler.codec.TooLongFrameException;
import com.apache.rich.servlet.common.utils.ValidatorUtils;
import com.apache.rich.servlet.http.servlet.realize.RichServletHttpServletWebapp;
import com.apache.rich.servlet.http.servlet.realize.interceptor.RichServletInterceptor;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.HttpUtil;
import static io.netty.handler.codec.http.HttpResponseStatus.CONTINUE;
import static io.netty.handler.codec.http.HttpVersion.HTTP_1_1;
import com.apache.rich.servlet.core.server.rest.response.RichServletServerHttpResponseBuilder;
import com.apache.rich.servlet.core.server.rest.response.RichServletServerHttpResponse;
import com.apache.rich.servlet.core.server.acceptor.AsyncRequestHandler;
import com.apache.rich.servlet.core.server.rest.request.NettyHttpRequest;
import com.apache.rich.servlet.core.server.rest.HttpContext;
import com.apache.rich.servlet.core.server.monitor.RichServletServerMonitor;
import io.netty.handler.codec.http.DefaultHttpResponse;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.timeout.ReadTimeoutException;

/**
 * 
 * @author wanghailing
 *
 */
public class RichServletAsyncHttpServletHandler extends AsyncRequestHandler {

	// Http context
	private volatile HttpContext httpContext;
	// channel context
	private ChannelHandlerContext context;

	private List<RichServletInterceptor> interceptors;

	/**
	 * Which uri should be passed into this servlet container
	 */
	private static String uriPrefix = "/";

	public static RichServletAsyncHttpServletHandler build(String prefix) {
		if (!ValidatorUtils.isBlank(prefix)) {
			uriPrefix = prefix;
		}
		return new RichServletAsyncHttpServletHandler();
	}

	public RichServletAsyncHttpServletHandler addInterceptor(
			RichServletInterceptor interceptor) {

		if (interceptors == null) {
			interceptors = new ArrayList<RichServletInterceptor>();
		}
		interceptors.add(interceptor);
		return this;
	}

	@Override
	protected void channelRead0(final ChannelHandlerContext ctx,
			final FullHttpRequest httpRequest) throws Exception {
		if (HttpUtil.is100ContinueExpected(httpRequest)) {
			ctx.writeAndFlush(new DefaultFullHttpResponse(HTTP_1_1, CONTINUE));
		}
		this.context = ctx;

		// build context
		NettyHttpRequest nettyHttpRequest=new NettyHttpRequest(context.channel(),
				httpRequest);
		httpContext = HttpContext.build(nettyHttpRequest);

		if (httpContext.getUri().startsWith(uriPrefix)) {
			RichservletHttpFilterChain chain = RichServletHttpServletWebapp.get().initializeChain(httpContext.getUri());
			if (chain.isValid()) {
                handleHttpServletRequest(ctx, httpRequest, chain);
            } else if (RichServletHttpServletWebapp.get().getStaticResourcesFolder() != null) {
                handleStaticResourceRequest(ctx, httpRequest);
            } else {
            		System.out.println("No handler found for uri");
                throw new RuntimeException(
                        "No handler found for uri: " + httpContext.getUri());
            }
		}else {
            ctx.fireChannelRead(httpRequest);
        }
	}

	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		RichServletHttpServletWebapp.get().getSharedChannelGroup()
				.add(ctx.channel());
		super.channelActive(ctx);
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
			throws Exception {
		if (cause instanceof ReadTimeoutException) {
			// httpcode 504

			RichServletServerHttpResponse hServerHttpResponse = new RichServletServerHttpResponse();
			hServerHttpResponse
					.setHttpResponse(RichServletServerHttpResponseBuilder
							.create(httpContext,
									HttpResponseStatus.GATEWAY_TIMEOUT));
			httpContext.setThrowable(cause);
			sendResponse(httpContext, hServerHttpResponse);

		} else if (cause instanceof IllegalArgumentException) {
			context.channel().close();
		} else if (context.channel().isActive()) {
			RichServletServerHttpResponse hServerHttpResponse = new RichServletServerHttpResponse();
			httpContext.setThrowable(cause);
			hServerHttpResponse
					.setHttpResponse(RichServletServerHttpResponseBuilder
							.create(httpContext,
									HttpResponseStatus.INTERNAL_SERVER_ERROR));
			sendResponse(httpContext, hServerHttpResponse);
		} else if (cause instanceof TooLongFrameException) {
			RichServletServerHttpResponse hServerHttpResponse = new RichServletServerHttpResponse();
			httpContext.setThrowable(cause);

			hServerHttpResponse
					.setHttpResponse(RichServletServerHttpResponseBuilder
							.create(httpContext, HttpResponseStatus.BAD_REQUEST));
			sendResponse(httpContext, hServerHttpResponse);
		}
	}

	
	private RichServletServerHttpResponse failure() {
		RichServletServerHttpResponse hServerHttpResponse = new RichServletServerHttpResponse();
		RichServletServerMonitor.setLastServFailID(httpContext.getRequestId());
		// httpcode 503
		DefaultHttpResponse httpResponse = RichServletServerHttpResponseBuilder
				.create(httpContext, HttpResponseStatus.SERVICE_UNAVAILABLE);
		hServerHttpResponse.setHttpResponse(httpResponse);
		return hServerHttpResponse;
	}

	private void sendResponse(HttpContext ctx,
			RichServletServerHttpResponse response) {
		if (context.channel().isOpen()) {
			ChannelFuture future = context.channel().writeAndFlush(
					response.getHttpResponse());
			// http short connection
			if (!ctx.isKeepAlive()
					|| response.getHttpResponse().status()
							.equals(HttpResponseStatus.OK)) {
				future.addListener(ChannelFutureListener.CLOSE);
			}
		}

	}

	protected RichServletHttpServletResponse buildHttpServletResponse(
			FullHttpResponse response) {
		return new RichServletHttpServletResponse(response);
	}
	
	/**
	 * 处理http 请求
	 * @param ctx
	 * @param request
	 * @param chain
	 * @throws Exception
	 */
	protected void handleHttpServletRequest(ChannelHandlerContext ctx,
			HttpRequest request, RichservletHttpFilterChain chain) throws Exception {

		interceptOnRequestReceived(ctx, request);

		DefaultFullHttpResponse response = new DefaultFullHttpResponse(
				HTTP_1_1, OK);

		RichServletHttpServletResponse resp = buildHttpServletResponse(response);
		RichServletHttpServletRequest req = buildHttpServletRequest(request, chain);

		chain.doFilter(req, resp);

		interceptOnRequestSuccessed(ctx, request, response);

		resp.getWriter().flush();

		boolean keepAlive = HttpUtil.isKeepAlive(request);
		if (keepAlive) {
			// Add 'Content-Length' header only for a keep-alive connection.
			response.headers().set(HttpHeaderNames.CONTENT_LENGTH,
					response.content().readableBytes());
			// Add keep alive header as per:
			// -
			// http://www.w3.org/Protocols/HTTP/1.1/draft-ietf-http-v11-spec-01.html#Connection
			response.headers().set(HttpHeaderNames.CONNECTION, HttpHeaderValues.KEEP_ALIVE);
			response.headers().set(HttpConstants.HEADER_REQUEST_ID, httpContext.getRequestId());
			response.headers().set(HttpConstants.HEADER_SERVER, RichServletServerConstants.HTTP_SERVER);
			response.headers().set(HttpConstants.HEADER_CONNECTION, HttpConstants.HEADER_CONNECTION_KEEPALIVE);
			response.headers().set(HttpConstants.ACCESS_CONTROL_ALLOW_ORIGIN, "*");
			response.headers().set(HttpConstants.CACHE_CONTROL,"no-cache");
			response.headers().set(HttpConstants.PRAGMA,"no-cache");
			response.headers().set(HttpConstants.EXPIRES,"-1");
		}
		RichServletServerHttpResponse hServerHttpResponse = new RichServletServerHttpResponse();
		hServerHttpResponse.setHttpResponse(response);
		sendResponse(httpContext, hServerHttpResponse);

	}

	/**
	 * 处理http 静态资源
	 * 
	 * @param ctx
	 * @param request
	 * @throws Exception
	 */
	protected void handleStaticResourceRequest(ChannelHandlerContext ctx,
			HttpRequest request) throws Exception {
		if (request.method() != HttpMethod.GET) {
			RichServletServerHttpResponse hServerHttpResponse = new RichServletServerHttpResponse();
			hServerHttpResponse
					.setHttpResponse(RichServletServerHttpResponseBuilder
							.create(httpContext,
									HttpResponseStatus.METHOD_NOT_ALLOWED));
			sendResponse(httpContext, hServerHttpResponse);
			return;
		}

		String uri = HttpServletUtils.sanitizeUri(request.uri());
		final String path = (uri != null ? RichServletHttpServletWebapp.get()
				.getStaticResourcesFolder().getAbsolutePath()
				+ File.separator + uri : null);

		if (path == null) {
			RichServletServerHttpResponse hServerHttpResponse = new RichServletServerHttpResponse();
			hServerHttpResponse
					.setHttpResponse(RichServletServerHttpResponseBuilder
							.create(httpContext, HttpResponseStatus.FORBIDDEN));
			sendResponse(httpContext, hServerHttpResponse);
			return;
		}

		File file = new File(path);
		if (file.isHidden() || !file.exists()) {
			RichServletServerHttpResponse hServerHttpResponse = new RichServletServerHttpResponse();
			hServerHttpResponse
					.setHttpResponse(RichServletServerHttpResponseBuilder
							.create(httpContext, HttpResponseStatus.NOT_FOUND));
			sendResponse(httpContext, hServerHttpResponse);
			return;
		}
		if (!file.isFile()) {
			RichServletServerHttpResponse hServerHttpResponse = new RichServletServerHttpResponse();
			hServerHttpResponse
					.setHttpResponse(RichServletServerHttpResponseBuilder
							.create(httpContext, HttpResponseStatus.FORBIDDEN));
			sendResponse(httpContext, hServerHttpResponse);
			return;
		}

		RandomAccessFile raf;
		try {
			raf = new RandomAccessFile(file, "r");
		} catch (FileNotFoundException fnfe) {
			RichServletServerHttpResponse hServerHttpResponse = new RichServletServerHttpResponse();
			hServerHttpResponse
					.setHttpResponse(RichServletServerHttpResponseBuilder
							.create(httpContext, HttpResponseStatus.NOT_FOUND));
			sendResponse(httpContext, hServerHttpResponse);
			return;
		}

		long fileLength = raf.length();

		HttpResponse response = new DefaultHttpResponse(HTTP_1_1,
				HttpResponseStatus.OK);
		response.headers().set(HttpConstants.HEADER_CONTENT_LENGTH, fileLength);

		Channel ch = ctx.channel();

		// Write the initial line and the header.
		ch.write(response);

		// Write the content.
		ChannelFuture writeFuture;
		if (isSslChannel(ch)) {
			// Cannot use zero-copy with HTTPS.
			writeFuture = ch.writeAndFlush(new ChunkedFile(raf, 0, fileLength,
					8192));
		} else {
			// No encryption - use zero-copy.
			final FileRegion region = new DefaultFileRegion(raf.getChannel(),
					0, fileLength);
			writeFuture = ch.writeAndFlush(region);
			writeFuture.addListener(new ChannelProgressiveFutureListener() {

				@Override
				public void operationProgressed(
						ChannelProgressiveFuture channelProgressiveFuture,
						long current, long total) throws Exception {

					System.out.printf("%s: %d / %d (+%d)%n", path, current,
							total, total);
				}

				@Override
				public void operationComplete(
						ChannelProgressiveFuture channelProgressiveFuture)
						throws Exception {
					region.release();
				}
			});
		}

	}

	protected RichServletHttpServletRequest buildHttpServletRequest(
			HttpRequest request, RichservletHttpFilterChain chain) {
		return new RichServletHttpServletRequest(request, chain);
	}

	private boolean isSslChannel(Channel ch) {
		return ch.pipeline().get(SslHandler.class) != null;
	}

	public String getUriPrefix() {
		return uriPrefix;
	}

	public void setUriPrefix(String uriPrefix) {
		this.uriPrefix = uriPrefix;
	}

	private void interceptOnRequestReceived(ChannelHandlerContext ctx,
			HttpRequest request) {
		if (interceptors != null) {
			for (RichServletInterceptor interceptor : interceptors) {
				interceptor.onRequestReceived(ctx, request);
			}
		}

	}

	private void interceptOnRequestSuccessed(ChannelHandlerContext ctx,
			HttpRequest request, HttpResponse response) {
		if (interceptors != null) {
			for (RichServletInterceptor interceptor : interceptors) {
				interceptor.onRequestSuccessed(ctx, request, response);
			}
		}
	}
}
