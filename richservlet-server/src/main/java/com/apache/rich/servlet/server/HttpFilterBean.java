package com.apache.rich.servlet.server;

import java.io.Serializable;

import javax.servlet.Filter;

public class HttpFilterBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7077634988318372391L;
	
	private Class<? extends Filter> filter;
	
	private String filterpath;
	
	/**
	 * @return the filter
	 */
	public Class<? extends Filter> getFilter() {
		return filter;
	}

	/**
	 * @param filter the filter to set
	 */
	public void setFilter(Class<? extends Filter> filter) {
		this.filter = filter;
	}

	/**
	 * @return the filterpath
	 */
	public String getFilterpath() {
		return filterpath;
	}

	/**
	 * @param filterpath the filterpath to set
	 */
	public void setFilterpath(String filterpath) {
		this.filterpath = filterpath;
	}
	
	
}
