package com.apache.rich.servlet.core.server.rest.interceptor;

import com.apache.rich.servlet.core.server.rest.response.RichServletServerHttpResponse;

import com.apache.rich.servlet.core.server.servlet.HttpSession;
import io.netty.handler.codec.http.DefaultHttpContent;

/**
 * http 请求拦截器
 * @author wanghailing
 *
 */
public abstract class Interceptor {
	
	 /**
     * http context of request information. user can update
     * the value of context.
     *
     * @param context http context
     * @return true if we continue the request or false will deny the request by http code 403
     */
    public boolean filter(final HttpSession context) {
        return true;
    }

    /**
     * http context of request information. user can update
     * the value of context or change the response. don't accept null
     * returned. it will be ignored
     *
     * @param context  http context
     * @param response represent response
     * @return response new response instance or current Object instance
     */
    public RichServletServerHttpResponse handler(final HttpSession context, RichServletServerHttpResponse httpContent) {
        return httpContent;
    }
}
