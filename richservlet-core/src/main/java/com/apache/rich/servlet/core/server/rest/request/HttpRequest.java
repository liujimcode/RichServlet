package com.apache.rich.servlet.core.server.rest.request;

import io.netty.handler.codec.http.HttpVersion;

import java.util.Map;

import com.apache.rich.servlet.common.enums.RequestMethodEnums;

/**
 * http request访问请求接口
 * @author wanghailing
 *
 */
public interface HttpRequest {
    String visitRemoteAddress();

    String visitURI();

    String[] visitTerms();

    RequestMethodEnums visitHttpMethod();

    String visitHttpBody();

    Map<String, String> visitHttpParams();

    Map<String, String> visitHttpHeaders();

    HttpVersion visitHttpVersion();
}
