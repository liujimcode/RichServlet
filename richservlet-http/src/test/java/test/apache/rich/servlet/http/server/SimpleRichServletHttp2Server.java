/**
 * 
 */
package test.apache.rich.servlet.http.server;

import com.apache.rich.servlet.http2.server.RichServletHttp2ServerProvider;
import com.apache.rich.servlet.http2.server.RichServletHttp2Server;
import com.apache.rich.servlet.core.server.helper.RichServletServerOptions;
import com.apache.rich.servlet.core.server.monitor.RichServletServerMonitor;

/**
 * @author wanghailing
 *
 */
public class SimpleRichServletHttp2Server {

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		RichServletHttp2Server richServletHttp2Server=RichServletHttp2ServerProvider.getInstance().service();
		// disable internal root path stats controller
		richServletHttp2Server.disableInternalController();
        // disable stats monitor
        RichServletServerMonitor.disable();

        // 2. choose http params. this is unnecessary
        richServletHttp2Server.option(RichServletServerOptions.IO_THREADS, Runtime.getRuntime().availableProcessors())
                .option(RichServletServerOptions.WORKER_THREADS, 128)
                .option(RichServletServerOptions.TCP_BACKLOG, 1024)
                .option(RichServletServerOptions.TCP_NODELAY, true)
                .option(RichServletServerOptions.TCP_TIMEOUT, 10000L)
                //.option(RichServletServerOptions.TCP_HOST,"127.0.0.1")
                .option(RichServletServerOptions.TCP_PORT,8082)
                .option(RichServletServerOptions.MAX_CONNETIONS, 4096);

        richServletHttp2Server.scanHttpController("test.apache.rich.servlet.http.server.rest");

        // 3. start http server
        if (!richServletHttp2Server.start()){
            System.err.println("HttpServer run failed");
        } 
        try {
            // join and wait here
        		richServletHttp2Server.join();
        		richServletHttp2Server.shutdown();
        } catch (InterruptedException ignored) {
        }
	}

}
