package com.apache.rich.servlet.core.server.rest.response;

import io.netty.handler.codec.http.DefaultHttpContent;

import io.netty.handler.codec.http.DefaultHttpResponse;

public class RichServletServerHttpResponse {
	
	private DefaultHttpResponse httpResponse;
	
	private DefaultHttpContent httpContent;

	/**
	 * @return the httpResponse
	 */
	public DefaultHttpResponse getHttpResponse() {
		return httpResponse;
	}

	/**
	 * @param httpResponse the httpResponse to set
	 */
	public void setHttpResponse(DefaultHttpResponse httpResponse) {
		this.httpResponse = httpResponse;
	}

	/**
	 * @return the httpContent
	 */
	public DefaultHttpContent getHttpContent() {
		return httpContent;
	}

	/**
	 * @param httpContent the httpContent to set
	 */
	public void setHttpContent(DefaultHttpContent httpContent) {
		this.httpContent = httpContent;
	}
	
	
}
