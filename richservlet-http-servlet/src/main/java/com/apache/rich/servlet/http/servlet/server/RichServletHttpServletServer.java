package com.apache.rich.servlet.http.servlet.server;

import com.apache.rich.servlet.core.server.acceptor.IOAcceptor;
import com.apache.rich.servlet.core.server.RichServletServer;
/**
 * 
 * @author wanghailing
 *
 */
public class RichServletHttpServletServer extends RichServletServer{
	
	private IOAcceptor ioAcceptor;
	
	public RichServletHttpServletServer ioAcceptor(IOAcceptor asyncAcceptor) {
        this.ioAcceptor = asyncAcceptor;
        return this;
    }
	@Override
	public Boolean start() {
		try {
            // run event loop and joined
            ioAcceptor.eventLoop();
            return Boolean.TRUE;
        } catch (Exception e) {
            e.printStackTrace();
            return Boolean.FALSE;
        }
	}

	@Override
	public void join() throws InterruptedException {
        ioAcceptor.join();
	}

	@Override
	public void shutdown() {
        ioAcceptor.shutdown();
	}

}
