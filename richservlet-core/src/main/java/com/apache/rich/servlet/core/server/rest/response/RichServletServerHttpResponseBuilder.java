package com.apache.rich.servlet.core.server.rest.response;

import com.apache.rich.servlet.core.server.error.html.HtmlCreator;

import com.apache.rich.servlet.core.server.rest.HttpContext;
import com.apache.rich.servlet.common.constants.RichServletServerConstants;
import com.apache.rich.servlet.common.constants.HttpConstants;
import io.netty.buffer.Unpooled;
import io.netty.util.CharsetUtil;
import io.netty.handler.codec.http.DefaultHttpContent;
import io.netty.handler.codec.http.DefaultHttpResponse;
import io.netty.buffer.ByteBuf;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;

/**
 * Build an DefaultFullHttpResponse {@link DefaultFullHttpResponse}
 * @author wanghailing
 */
public class RichServletServerHttpResponseBuilder {


    public static DefaultFullHttpResponse create(HttpContext httpContext, HttpResponseStatus status) {
    		HtmlCreator htmlCreator = new HtmlCreator();
        htmlCreator.setTitle(status.code()+status.reasonPhrase());
        htmlCreator.setH1(status.code()+status.reasonPhrase());
        if(httpContext.getThrowable()==null){
            htmlCreator.addParagraph("The requested URL " + httpContext.getUri() +Thread.currentThread().getName()+ " was "+status.reasonPhrase()+" on this server.");
        }else{
            htmlCreator.addParagraph("The requested URL " + httpContext.getUri() + " was "+status.reasonPhrase()+",reason"+httpContext.getThrowable().getMessage()+" on this server.");

        }
        StringBuilder responseBuffer = new StringBuilder();
        responseBuffer.setLength(0); // clear StringBuilder
        responseBuffer.append(htmlCreator.getHtml());
    		return create(httpContext, status,Unpooled.copiedBuffer(responseBuffer.toString(), CharsetUtil.UTF_8));
    }

    public static DefaultFullHttpResponse create(HttpContext httpContext, ByteBuf content) {
        return create(httpContext, HttpResponseStatus.OK, content);
    }

    private static DefaultFullHttpResponse create(HttpContext httpContext, HttpResponseStatus status, ByteBuf content) {
    		DefaultFullHttpResponse resp = null;
        if (content != null) {
            resp = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, status,content);
            resp.headers().set(HttpConstants.HEADER_CONTENT_LENGTH, content.readableBytes());
            if(status.equals(HttpResponseStatus.OK)){
                resp.headers().set(HttpConstants.HEADER_CONTENT_TYPE, HttpConstants.HEADER_CONTENT_TYPE_JSON);
            }else{
                resp.headers().set(HttpConstants.HEADER_CONTENT_TYPE, HttpConstants.HEADER_CONTENT_TYPE_TEXT_PLAIN);
            }
        } 
        resp.headers().set(HttpConstants.HEADER_REQUEST_ID, httpContext.getRequestId());
        resp.headers().set(HttpConstants.HEADER_SERVER, RichServletServerConstants.HTTP_SERVER);
        resp.headers().set(HttpConstants.HEADER_CONNECTION, HttpConstants.HEADER_CONNECTION_KEEPALIVE);
        resp.headers().set(HttpConstants.ACCESS_CONTROL_ALLOW_ORIGIN, "*");
        resp.headers().set(HttpConstants.CACHE_CONTROL,"no-cache");
        resp.headers().set(HttpConstants.PRAGMA,"no-cache");
        resp.headers().set(HttpConstants.EXPIRES,"-1");
        return resp;
    }
    
    
}
